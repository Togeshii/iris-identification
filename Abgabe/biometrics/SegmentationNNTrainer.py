from keras_segmentation.models.unet import vgg_unet

"""
    This is a simple script to train a segmentation model based on a manually created dataset. 
    It uses the python library "keras-segmentation" which offers an easy interface for training segmentation models.

"""

# Create the model based on VGG UNet
model = vgg_unet(n_classes=2 ,  input_height=128, input_width=128  )

# Train the model based on the specified dataset
model.train(
    train_images =  "../data/SegmentationNNDataset/training/images",
    train_annotations = "../data/SegmentationNNDataset/training/masks",
    checkpoints_path = "../data/tmp" , epochs=5
)

# Saves the weights of the model to a file so it can be used at a later point
model.save("../data/models/segmentationModel.h5")
print("Saved Model to file.")

# Run a prediction as test to visualize the performance of the network
out = model.predict_segmentation(
    inp="../data/SegmentationNNDataset/test/images/573.png",
    out_fname="../data/tmp/out.png"
)

# Also show the output image
import matplotlib.pyplot as plt
plt.imshow(out)