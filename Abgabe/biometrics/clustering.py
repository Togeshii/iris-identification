import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

class Clustering:

    """
    Class clusters points from feature_extraction and saves cluster-centers
    :arg points (np.array): list of points
    """
    def __init__(self,points):
        self.points=points

    #clustering by kmeans method
    def k_means(self, points, visualize=False):
        if visualize:
            print('start K-MEANS')

            print(points)
            plt.scatter(points[:, 0], points[:, 1])
            plt.show()

            print('clustercenters____________')

        #Kmeans
        cluster_centers = []
        if len(points) > 8:
            k_means= KMeans().fit(points)

            #get cluster centers
            cluster_centers = k_means.cluster_centers_

        if visualize:
            plt.scatter(cluster_centers[:, 0], cluster_centers[:, 1])
            plt.show()

        return cluster_centers

