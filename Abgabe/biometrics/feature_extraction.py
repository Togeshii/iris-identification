import numpy as np
import cv2 as cv
import tqdm

"""
    This is a wrapper class to easily extract features from a list of images and return them in form of a dictionary.
    The Feature extractor can either be SIFT or KAZE. Per default SIFT is commented out, as it only works when
    compiling opencv from sources while including the opencv-contrib module.
"""
class FeatureExtraction:

    """
        Constructor that initializes the feature extractor.
    """
    def __init__(self):
        # Sift required own compiled opencv version that includes the opencv-contrib modules
        #self.sift = cv.xfeatures2d.SIFT_create(nfeatures=1000, nOctaveLayers=10, contrastThreshold=0.001, edgeThreshold=10, sigma=1.7) # The smaller sigma gets, the more features.

        # KAZE is free
        self.kaze = cv.KAZE_create(threshold=0.0001, nOctaves=7, nOctaveLayers=7)

    """
        Method that extracts
        :arg pictures (list): list of pictures as paths
        :returns A dictionary that contains the picture name and it's extracted keypoints
    """
    def extract(self, pictures, visualize=False):
        print("Extracting features")

        # init output dictionary
        output_dict = {}

        for i in tqdm.tqdm(range(len(pictures))):
            pic = pictures[i]

            # Load the image
            image = cv.imread(pic)

            # Run Feature detection
            #features = sift.detect(image, None)
            features = self.kaze.detect(image, None)

            # Print number of features
            #print("Found " + str(len(features)) + " features for image " + str(pic))

            # Fill an array with the coordinates
            points=[]
            for f in features:
                points.append(f.pt)

            # Save the points into the dictionary
            output_dict[pic.split("/")[-1]] = points

            # visualization
            if visualize:
                img = cv.drawKeypoints(image, features, image)
                cv.imshow("Keypoints", img)

                cv.waitKey(0)

        return output_dict