import numpy as np
import cv2
from matplotlib import pyplot as plt
from PIL import Image

class Preprocessing:

    #Finds first the face and than the eyes in the given image
    def haarcascade(self, img):
        
        #pretrained classifiers
        face_cascade = cv2.CascadeClassifier('../data/models/haarcascade_frontalface_default.xml')
        eye_cascade = cv2.CascadeClassifier('../data/models/haarcascade_eye.xml')


        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)

        left_eye_img = np.zeros_like(img)
        for (x, y, w, h) in faces:
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

            roi_gray = gray[y:y + h, x:x + w]
            roi_color = img[y:y + h, x:x + w]
            eyes = eye_cascade.detectMultiScale(roi_gray)

            left_eyes_List = []
            for (ex, ey, ew, eh) in eyes:
                #find eye in left half of the image
                if ex <  roi_gray.shape[0]/2:
                    left_eyes_List.append((ex, ey, ew, eh))

                cv2.rectangle(roi_color, (ex - 2, ey - 2), (ex + ew + 4 , ey + eh + 4), (0, 255, 0), 2)
            (ex, ey, ew, eh) = left_eyes_List[0]
            left_eye = (ex, ey, ew, eh)
            #find the 'highest eye'
            for (ex, ey, ew, eh) in left_eyes_List:

                if ey < left_eye[1]:
                    left_eye = (ex, ey, ew, eh)

            (ex, ey, ew, eh) = left_eye
            cv2.rectangle(roi_color, (ex - 2, ey - 2), (ex + ew + 4, ey + eh + 4), (0, 0, 255), 2)

            #cut out left eye
            left_eye_img = img[y+ey:y+eh+ey, x+ex:x+ ew+ex, :]


        cv2.imshow('img', img)
        if left_eye_img.shape == img.shape:
            left_eye_img = None
            print('No face found, take new picture!')
        else:
            cv2.imshow('left eye', left_eye_img)

        #return img and left_eye extraction
        return img, left_eye_img

    #histogram equalization with clahe
    def histogram_equalization(self, images):

        for i in range(0, len(images)):

            #load image
            img = np.array(Image.open(images[i]).convert('L'))

            #using clahe
            clahe = cv2.createCLAHE(clipLimit=2,tileGridSize=(8, 8) )
            img = clahe.apply(img)

            #creating copy of clahe version
            path = images[i].replace('.png', '_clahe.png')

            cv2.imwrite(path, img)
            #new path in images array
            images[i] = path
        return images





