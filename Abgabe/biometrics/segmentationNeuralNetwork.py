from PIL import Image, ImageOps
import cv2
from keras_segmentation.models.unet import vgg_unet

"""
    Python class that loads a pretrained segmentation model that is able to create a mask to extract only the iris
    from an Image. The segmentation model is generated and trained in the "SegmentationNNTrainer" class.
"""
class Segmentation:

    """
        Load the keras model that is based von VGG UNet.
    """
    def loadModel(self):
        # Load the model
        self.model = vgg_unet(n_classes=2 ,  input_height=128, input_width=128  )
        self.model.load_weights("../data/models/segmentationModel.h5")

    """
        Applies the segmentation model to a single image.
        :arg imagePath: The path to the image that needs to be segmented.
        :arg outputPath: Where to save the segmented image.
    """
    def predict(self, imagePath, outputPath):
        # First rescale the image for input into the network
        image = Image.open(imagePath)
        size = (128, 128)
        image = ImageOps.fit(image, size, Image.ANTIALIAS)

        # Save the rescaled image to a temporary location
        image.save("../data/tmp/resized.png", "PNG")

        # Apply the segmentation and save the mask to a temporary location
        self.model.predict_segmentation(
            inp="../data/tmp/resized.png",
            out_fname="../data/tmp/mask.png"
        )

        # Load the image and mask
        img = cv2.imread("../data/tmp/resized.png")
        mask = cv2.imread("../data/tmp/mask.png")
        mask_gray = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)

        # Threshold the mask, so that the iris is marked in white and the rest in black
        ret, mask_bw = cv2.threshold(mask_gray, 170, 255, cv2.THRESH_BINARY)

        # First roughly loop over the image to calculate the average color. This helps with histogram equalization.
        summed_brightness = 0
        count = 0

        # Loop in steps of 5
        for y in range(0, img.shape[0], 5):
            for x in range(0, img.shape[1], 5):
                # Only consider the pixel if it is part of the iris
                if mask_bw[y, x] > 50:
                    summed_brightness += img[y, x, 0]
                    summed_brightness += img[y, x, 1]
                    summed_brightness += img[y, x, 2]
                    count += 3

        # Calculate the average brightness
        avg = summed_brightness/count

        # loop over the image, pixel by pixel
        for y in range(0, img.shape[0]):
            for x in range(0, img.shape[1]):
                if mask_bw[y, x] < 50:
                    img[y, x] = (avg, avg, avg)

        # Display the result
        cv2.imshow('Segmented Image', img)
        #cv2.waitKey(0)

        # Also save the result to the specified output path
        cv2.imwrite(outputPath, img)




if __name__ == '__main__':
    # Generate an instance of the class
    seg = Segmentation()

    # Load the keras model into the runtime
    seg.loadModel()

    # Run the segmentation on a test image
    seg.predict("../data/SegmentationNNDataset/test/images/731.png", "../data/tmp/test.png")

    cv2.waitKey(0)

