This is the dataset we used to train the neural network that is used for Attack detection. 
It was created from 1417 webcam images that either captured a real subject or an attack.
The attacks were in form of either a printed picture of a person or a picture of a person on a smartphone display.
From each image the left eye was extracted. The resulting patches were scaled to 128x128 pixels.
The dataset was split into a training set, a validation set, and a test set.