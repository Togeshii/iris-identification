This is the dataset we used to train the neuronal network for segmentation. It consists of a number of images with corresponding masks.
The masks were created manually with the "PixelAnnotationTool v1.4.0" from https://github.com/abreheret/PixelAnnotationTool.
It is based of images from the "KerasDatasets_PAD_Big" dataset as well as images from the UBIRIS.v2 dataset.