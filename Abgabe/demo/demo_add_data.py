import numpy as np
import cv2
from cv2 import *
from biometrics import preprocessing, feature_extraction, clustering, matching, segmentationNeuralNetwork
import pickle
from biometrics.attack_detector import AttackDetector
import os

def main():
    # Step 1: Extract webcam image

    # take a snapshot when you hit space, quit if you hit escape
    cam = cv2.VideoCapture(0)

    cv2.namedWindow("livedemo")

    img_counter = 0
    # preprocessing class
    prepro = preprocessing.Preprocessing()

    # Attack detector class
    ad = AttackDetector()
    ad.loadModel()

    # Initialize the segmentation module
    seg = segmentationNeuralNetwork.Segmentation()
    seg.loadModel()

    # Initialize the feature extractor
    extractor = feature_extraction.FeatureExtraction()

    # Initialize the custering module
    clustering_ex = clustering.Clustering(np.array([]))

    while True:
        ret, frame = cam.read()
        cv2.imshow("Photo: press 'Space', close: press escape, add data: press '+' ", frame)
        if not ret:
            break
        k = cv2.waitKey(1)

        # terminate run
        if k % 256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            break

        # add to database
        elif k % 256 == 43:
            # if + was pressed
            datapath = '../data/cluster_database_kaze.pkl'
            datapath= '../data/cluster_database_kaze_demo.pkl'
            print("ESC: Return to Menu\nAny key: Take picture")
            in_k = cv2.waitKey(1)
            if (in_k % 256 == 27):
                print("Returning to menu")
                cv2.destroyAllWindows()
            else:
                img_name = "opencv_frame{}.png".format(img_counter)
                cv2.imwrite(img_name, frame)
                print("{} written".format(img_name))
                img_counter += 1

                # Augen finden und anzeigen
                img, left_eye = prepro.haarcascade(cv2.imread(img_name))
                if np.any(left_eye):
                    img_path = img_name.replace('.png', 'leftEYE.png')
                    cv2.imwrite(img_path, left_eye)

                    # Segmentierung
                    segmentedImageName = img_path.replace(".png", "_segmented.png")
                    # segmentation.mask_iris(img_path[0], segmentedImageName, "")
                    seg.predict(img_path, segmentedImageName)
                    cv2.imshow("segmentated image", imread(img_path))

                    print("Press SPACE to commence or any other key to cancel")
                    commence = cv2.waitKey(0)
                    if (commence % 256 == 32):
                        # Histogramausgleich
                        imgList = []
                        imgList.append(segmentedImageName)
                        img_path = prepro.histogram_equalization(imgList)

                        # name tag
                        tag = input("Enter tag name: ")
                        # Use the segmented image for feature extraction

                        keypoint_dict = extractor.extract(img_path, False)

                        points_pic = keypoint_dict[img_path[0]]
                        centers = clustering_ex.k_means(np.array(points_pic))

                        # add to database
                        data = {}
                        if os.path.exists(datapath):
                            with open(datapath, 'rb') as f:
                                data = pickle.load(f)
                        data[tag]=centers
                        with open(datapath, 'wb') as f:
                            pickle.dump(data,f)

                        print("Datbase length " + str(len(data)))

                        print(tag, "was added to database. Returning to menu")
                        cv2.destroyAllWindows()

                    else:
                        print("Redirecting to Menu")
                        cv2.destroyAllWindows()

        # identify person
        elif k % 256 == 32:
            # SPACE pressed
            img_name = "opencv_frame_{}.png".format(img_counter)
            cv2.imwrite(img_name, frame)
            print("{} written!".format(img_name))
            img_counter += 1

            # Step 2: Find and show eye
            # find eyepatch and left eye
            img, left_eye = prepro.haarcascade(cv2.imread(img_name))
            # check if right_eye is not empty
            if np.any(left_eye):
                img_path = img_name.replace('.png', 'leftEYE.png')
                cv2.imwrite(img_path, left_eye)
                # Attack detection
                # ad.predict(img_name.replace('.png', 'leftEYE.png'))
                ad.predict(img_path)

                # Segmentation
                segmentedImageName = img_path.replace(".png", "_segmented.png")
                # segmentation.mask_iris(img_path[0], segmentedImageName, "")
                seg.predict(img_path, segmentedImageName)

                imgList = []
                # Histogramausgleich
                imgList.append(segmentedImageName)
                img_path = prepro.histogram_equalization(imgList)

                # Show eye with clahe
                bigger_img = imread(img_path[0])
                bigger_img = cv2.resize(bigger_img, (bigger_img.shape[0] * 2, bigger_img.shape[1] * 2))
                cv2.imshow('Left eye with clahe preprocessing', bigger_img)

                keypoint_dict = extractor.extract(img_path, False)

                # Clustering
                points_pic = keypoint_dict[img_path[0]]
                centers = clustering_ex.k_means(np.array(points_pic))

                print("Centers = " + str(centers))

                # matching
                match = matching.find_match(centers, datapath='../data/cluster_database_kaze_demo.pkl')
                if len(match) > 1:
                    print("Found Match:", match[match.shape[0] - 1, 0], " Confidence:", match[match.shape[0] - 1, 1],
                          "%")
                else:
                    print("No valid match found!")

                cv2.waitKey(0)
                cv2.destroyAllWindows()


    cam.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
