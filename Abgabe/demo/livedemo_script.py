import numpy as np
import cv2
from cv2 import *
from biometrics import preprocessing, feature_extraction, clustering, matching, segmentationNeuralNetwork, attack_detector
import pickle


def main():
    # Schritt1: Kamerabild extrahieren + möglicherweise Anweisungen geben

    # take a snapshot when you hit space, quit if you hit escape
    cam = cv2.VideoCapture(0)

    cv2.namedWindow("livedemo")

    img_counter = 0
    # preprocessing class
    prepro = preprocessing.Preprocessing()

    # Attack detector class
    ad = attack_detector.AttackDetector()
    ad.loadModel()

    # Initialize the feature extractor
    extractor = feature_extraction.FeatureExtraction()

    # Initialize the custering module
    clustering_ex = clustering.Clustering(np.array([]))

    # Initialize the segmentation module
    seg = segmentationNeuralNetwork.Segmentation()
    seg.loadModel()

    while True:
        ret, frame = cam.read()
        cv2.imshow("Photo: press 'Space', close: press escape ", frame)
        if not ret:
            break
        k = cv2.waitKey(1)

        if k % 256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            break
        elif k % 256 == 32:
            # SPACE pressed
            img_name = "opencv_frame_{}.png".format(img_counter)
            cv2.imwrite(img_name, frame)
            print("{} written!".format(img_name))
            img_counter += 1

            # Schritt2: Augen finden und anzeigen
            # find eyepatch and left eye
            img, left_eye = prepro.haarcascade(cv2.imread(img_name))


            #check if left_eye is not empty
            if np.any(left_eye):
                img_path = img_name.replace('.png', 'leftEYE.png')
                cv2.imwrite(img_path, left_eye)
                # Attack detection

                ad.predict(img_path)

                # Segmentierung
                segmentedImageName = img_path.replace(".png", "_segmented.png")
                seg.predict(img_path, segmentedImageName)

                imgList = []
                #HistogramAusgleich
                imgList.append(segmentedImageName)
                img_path =  prepro.histogram_equalization(imgList)


                # Show eye with clahe
                bigger_img = imread(img_path[0])
                bigger_img= cv2.resize(bigger_img, (bigger_img.shape[0]*2, bigger_img.shape[1]*2))
                cv2.imshow('Left eye with clahe preprocessing', bigger_img)


                keypoint_dict = extractor.extract(img_path, False)

                # save the keypoint dictionary
                # with open('../demo/' + featuresFile + '.pkl', 'wb') as f:
                #    pickle.dump(keypoint_dict, f, pickle.HIGHEST_PROTOCOL)

               #Clustering
                points_pic = keypoint_dict[img_path[0]]
                centers = clustering_ex.k_means(np.array(points_pic))

                print("Centers = " + str(centers))

                #matching
                #match = matching.find_match(centers, datapath="../data/small_cluster_database_kaze.pkl", threshold=0.3)
                match = matching.find_match(centers, datapath='../data/cluster_database_kaze_demo.pkl')
                if len(match) > 1:
                    print("Found Match:", match[match.shape[0]-1,0], " Confidence:", match[match.shape[0]-1,1],"%")
                    print("Found Match:", match[match.shape[0]-1,0], " Confidence:", match[match.shape[0]-1,1],"%")
                else:
                    print("No valid match found!")

                cv2.waitKey(0)
                cv2.destroyAllWindows()




    cam.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
