import os
from os import listdir
from os.path import isfile, join, isdir
import cv2
import numpy as np
from PIL import Image, ImageOps

path2="../data/SegmentationNNDataset/training/images"
imgs = [f for f in listdir(path2) if isfile(join(path2, f))]

print(imgs)

for image_path in imgs:
    # Convert the image to the resolution 224x224
    # load the image using PIL
    image = Image.open(path2 + "/" + image_path)

    # resize the image to a 224x224 with the same strategy as in TM2:
    # resizing the image to be at least 224x224 and then cropping from the center
    size = (128, 128)
    image = ImageOps.fit(image, size, Image.ANTIALIAS)

    image.save(path2 + "/" + image_path, "PNG")