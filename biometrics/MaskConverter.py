import os
from os import listdir
from os.path import isfile, join, isdir
import cv2
import numpy as np
from PIL import Image, ImageOps


datasetPath = "../data/ManualMasks"
masks = [f for f in listdir(datasetPath) if isfile(join(datasetPath, f)) and str(f)[-3:] == "png"]

for image_path in masks:
    img = cv2.imread(datasetPath + "/" + image_path)

    ann_img = np.zeros((img.shape[0], img.shape[1], 3)).astype('uint8')

    for x in range(img.shape[0]):
        for y in range(img.shape[1]):

            if img[x, y, 0] > 10:
                ann_img[x, y] = 1  # this would set the label of pixel 3,4 as 1

    cv2.imwrite("../data/ConvertedMasks/" + image_path.replace("_mask.png",".png"), ann_img)

    # Convert the image to the resolution 224x224
    # load the image using PIL
    image = Image.open("../data/ConvertedMasks/" + image_path.replace("_mask.png",".png"))

    # resize the image to a 224x224 with the same strategy as in TM2:
    # resizing the image to be at least 224x224 and then cropping from the center
    size = (128, 128)
    image = ImageOps.fit(image, size, Image.ANTIALIAS)

    image.save("../data/ConvertedMasks/" + image_path.replace("_mask.png",".png"), "PNG")


# Loop trough all images and rescale them

path2="../data/SegmentationNNDataset/imagesRaw"
imgs = [f for f in listdir(path2) if isfile(join(path2, f)) and str(f)[-3:] == "jpg"]

for image_path in imgs:
    # Convert the image to the resolution 224x224
    # load the image using PIL
    image = Image.open(path2 + "/" + image_path)

    # resize the image to a 224x224 with the same strategy as in TM2:
    # resizing the image to be at least 224x224 and then cropping from the center
    size = (128, 128)
    image = ImageOps.fit(image, size, Image.ANTIALIAS)

    image.save("../data/SegmentationNNDataset/images/" + image_path.replace(".jpg", ".png"), "PNG")