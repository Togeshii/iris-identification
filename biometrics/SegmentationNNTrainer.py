from keras_segmentation.models.unet import vgg_unet
from PIL import Image, ImageOps

model = vgg_unet(n_classes=2 ,  input_height=128, input_width=128  )

model.train(
    train_images =  "../data/SegmentationNNDataset/training/images",
    train_annotations = "../data/SegmentationNNDataset/training/masks",
    checkpoints_path = "../data/tmp" , epochs=5
)

model.save("segmentationModel.h5")
print("Saved Model to file.")

out = model.predict_segmentation(
    inp="../data/SegmentationNNDataset/test/images/573.png",
    out_fname="../data/tmp/out.png"
)

import matplotlib.pyplot as plt
plt.imshow(out)

# evaluating the model
print(model.evaluate_segmentation( inp_images_dir="../data/SegmentationNNDataset/test/images/"  , annotations_dir="../data/SegmentationNNDataset/test/masks/" ) )