import tensorflow as tf
from PIL import Image, ImageOps
import numpy as np
import cv2

"""
    Python class that loads a pretrained keras model that classifies the input as presentation attack.
    The code is based on the example code by "TeachableMachine" created by Google
"""
class AttackDetector:

    def loadModel(self):
        # Load the model
        self.model = tf.keras.models.load_model('../data/models/keras_modelV2.h5')

    def predict(self, imagePath, visualize=True):
        # Initialize an array with the correct input size
        data = np.ndarray(shape=(1, 224, 224, 3), dtype=np.float32)

        # load the image using PIL
        image = Image.open(imagePath)

        # resize the image to a 224x224 with the same strategy as in TM2:
        # resizing the image to be at least 224x224 and then cropping from the center
        size = (224, 224)
        image = ImageOps.fit(image, size, Image.ANTIALIAS)

        # turn the image into a numpy array
        image_array = np.asarray(image)

        # Normalize the image
        normalized_image_array = (image_array.astype(np.float32) / 127.0) - 1

        # Load the image into the array
        data[0] = normalized_image_array

        # run the inference
        labels = ["Real", "Fake"]
        prediction = self.model.predict(data)
        predicted_label = labels[int(np.argmax(prediction))]
        confidence = np.round(100*prediction[0, int(np.argmax(prediction))],3)
        print("Prediction: " + predicted_label +
              " [Confidence = " + str(confidence) + "%]")

        if visualize:
            # Reading an image in default mode
            image = 255*np.ones((110,200,3), np.uint8)


            # Using cv2.putText() method
            if int(np.argmax(prediction)) == 0:
                image = cv2.putText(image, 'Real', (35, 60), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 180, 0), 5, cv2.LINE_AA)
            else:
                image = cv2.putText(image, 'Fake', (28, 60), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 180), 5, cv2.LINE_AA)
            image = cv2.putText(image, 'Confidence: ' + str(confidence) + "%", (20, 90), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)

            # Displaying the image
            cv2.imshow("Attack Detection Result", image)
            #cv2.waitKey(0)


if __name__ == '__main__':
    ad = AttackDetector()
    ad.loadModel()
    ad.predict("../data/KerasDatasets_PAD_Big/test/fake/752.jpg")