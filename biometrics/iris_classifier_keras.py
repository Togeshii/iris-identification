import os
from os import listdir
from os.path import isfile, join, isdir
import cv2
import numpy as np
from tqdm import tqdm
from sklearn.cluster import KMeans
from keras import layers
from keras.models import Sequential
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import TensorBoard
import random
import shutil
from shutil import copyfile
from keras.applications import MobileNet
from keras.preprocessing import image
from keras.applications.mobilenet import preprocess_input
from keras.models import Model
from keras.optimizers import SGD
from keras.models import load_model

import datetime

import tensorflow as tf

def extract_patches_from_dataset(datasetPath):
    # Some settings
    DEBUG_VISUALIZE_REGIONS_OF_INTEREST = False
    DEBUG_VISUALIZE_EXTRACTED_EYES = False
    DEBUG_VISUALIZE_SCALED_AND_NORMALIZED_EYES = False
    DEBUG_VISUALIZE_KEYPOINTS = False
    DEBUG_VISUALIZE_CLUSTER_CENTERS = False

    # Use keypoints and clustering to extract patches around each cluster center. Probably not a good idea.
    USE_FANCY_PATCHES = False
    GRAYSCALE = False

    # Obtain a list of all training images
    images = [f for f in listdir(datasetPath) if isfile(join(datasetPath, f)) and str(f)[-4:] == "tiff"]


    print("Found " + str(len(images)) + " source images")

    # initialize the eye cascade classifier
    eye_cascade = cv2.CascadeClassifier('../data/haarcascade_eye.xml')

    # Initialize the clahe processor
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))

    # initialize the sift extractor
    sift = cv2.xfeatures2d.SIFT_create(nfeatures=1000, nOctaveLayers=5, contrastThreshold=0.01, edgeThreshold=10,
                                      sigma=2)  # The smaller sigma gets, the more features.

    # dictionary that saves how many images have been saved for each subject id
    subject_to_num = {}

    # For each image
    for img_id in tqdm(range(len(images))):
        img_name = images[img_id]

        # read the subject id:
        subject_id = str(img_name).split("_")[0][1:]

        # Filter only odd IDs to only keep the left eye
        if int(subject_id) % 2 != 0:
            continue



        # Load the image
        img = cv2.imread(datasetPath + "/" + img_name)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Extract all visible eyes using Haar cascade
        eyes = eye_cascade.detectMultiScale(gray)

        if len(eyes) == 0:
            continue

        # Calculate how many images have been processed for this subject
        image_id_of_subject = 0

        if "S" + str(subject_id) in subject_to_num:
            image_id_of_subject = subject_to_num["S" + str(subject_id)] + 1

        subject_to_num["S" + str(subject_id)] = image_id_of_subject

        # Initialize the selected eye
        eye = eyes[0]

        # If there are multiple eyes detected: Always select the biggest region
        if len(eyes) > 1:
            bestID = 0
            currentmax = -100
            for i in range(len(eyes)):
                if eyes[i][2] > currentmax:
                    currentmax = eyes[i][2]
                    bestID = i

            eye = eyes[bestID]

        # if the extracted region is too small: Just use the whole image as input
        if  eye[2] < 224 or eye[3] < 224:
            eye = [0, 0, img.shape[1], img.shape[0]]


        # Debug visualization
        if DEBUG_VISUALIZE_REGIONS_OF_INTEREST:
            cv2.rectangle(img, (eye[0], eye[1]), (eye[0] + eye[2], eye[1] + eye[3]), (0, 255, 0), 2)
            cv2.imshow("Region of interest", img)
            cv2.waitKey(0)

        # Extract the region of interest as pixel image
        eye_img = gray[eye[1]:eye[1]+eye[3], eye[0]:eye[0]+eye[2]]
        eye_col = img[eye[1]:eye[1] + eye[3], eye[0]:eye[0] + eye[2]]

        if DEBUG_VISUALIZE_EXTRACTED_EYES:
            cv2.imshow("Extracted Eye", eye_img)
            cv2.waitKey(0)

        if not USE_FANCY_PATCHES:
            # Scale the eye to a rectangular region measuring 128x128 pixels
            scaled = None

            # Check if the region is rectangular
            if eye_img.shape[0] == eye_img.shape[1]:
                # Only scale the image
                if GRAYSCALE:
                    scaled = cv2.resize(eye_img, (224, 224))
                else:
                    scaled = cv2.resize(eye_col, (224, 224))
            else:
                # crop the image and than scale it
                if GRAYSCALE:
                    cropped = eye_img[:, int(eye_img.shape[1]/2)-int(eye_img.shape[0]/2) : int(eye_img.shape[1]/2)+int(eye_img.shape[0]/2)]
                    scaled = cv2.resize(cropped, (224, 224))
                else:
                    cropped = eye_col[:,
                              int(eye_col.shape[1] / 2) - int(eye_img.shape[0] / 2): int(eye_img.shape[1] / 2) + int(
                                  eye_col.shape[0] / 2)]
                    scaled = cv2.resize(cropped, (224, 224))

            # Normalize the image
            if GRAYSCALE:
                normalized = clahe.apply(scaled)

            if DEBUG_VISUALIZE_SCALED_AND_NORMALIZED_EYES:
                cv2.imshow("Scaled and normalized eye", normalized)
                cv2.waitKey(0)

            # Save the image to a folder
            if GRAYSCALE:
                cv2.imwrite("../data/Patches/"+str(subject_id) + "_" + str(image_id_of_subject) + ".jpg", normalized)
            else:
                cv2.imwrite("../data/Patches/" + str(subject_id) + "_" + str(image_id_of_subject) + ".jpg", scaled)


        if USE_FANCY_PATCHES:
        
            # generate a mask using circle detection
            # Blur using 3 * 3 kernel.
            gray_blurred = cv2.blur(eye_img, (3, 3))

            # Apply Hough transform on the blurred image.
            detected_circles = cv2.HoughCircles(gray_blurred,
                                                cv2.HOUGH_GRADIENT, 1, 20, param1=50,
                                                param2=30, minRadius=int(eye[3]/8), maxRadius=int(2*eye[3]/3))

            # initialize the mask
            mask = np.ones(eye_img.shape, np.uint8)

            # Draw circles that are detected.
            if detected_circles is not None:
                # Convert the circle parameters a, b and r to integers.
                detected_circles = np.uint16(np.around(detected_circles))

                a, b, r = detected_circles[0][0][0], detected_circles[0][0][1], detected_circles[0][0][2]

                # Cause a black background
                cv2.circle(mask, (0, 0), 1000, (0, 0, 0), -1)

                # Draw the circumference of the circle.
                cv2.circle(mask, (a, b), r, (255, 255, 255), -1)


            # Normalize image before running sift extraction
            normalized = clahe.apply(eye_img)
            features = sift.detect(normalized, mask=mask)

            if DEBUG_VISUALIZE_KEYPOINTS:
                eye_col = cv2.drawKeypoints(eye_col, features, eye_col)
                cv2.imshow("Keypoints", eye_col)
                cv2.waitKey(0)

            # Run clustering
            # COnvert keypoints into points
            points = []
            for f in features:
                points.append(f.pt)

            # Kmeans
            k_means = KMeans(n_clusters=8).fit(np.array(points))

            # get cluster centers
            cluster_centers = k_means.cluster_centers_

            if DEBUG_VISUALIZE_CLUSTER_CENTERS:
                for c in cluster_centers:
                    cv2.circle(eye_col, (int(c[0]), int(c[1])), 2, (255, 0, 0), -1)

                cv2.imshow("Cluster Centers", eye_col)
                cv2.waitKey(0)

            # Extract a patch around each cluster center
            # Add the resulting patches to a dictionary containing a list of images for each subject

        # save all patches into a folder with a name corresponding to their subject

def convert_patches_into_sets():
    # Obtain a list of all training images
    patches = [f for f in listdir("../data/Patches") if isfile(join("../data/Patches", f)) and str(f)[-3:] == "jpg"]
    print("Found " + str(len(patches)) + " labeled patches")

    patches_dict = {}

    for patch in patches:
        # get the subject id from the patch name
        id = str(patch).split("_")[0]

        # Initialize the list of this subject if it is unknown
        if not id in patches_dict:
            patches_dict[id] = []

        patches_dict[id].append(patch)

    # Clear any previously existing folder structure
    if os.path.isdir('../data/KerasDatasets/training'):
        shutil.rmtree('../data/KerasDatasets/training')

    if os.path.isdir('../data/KerasDatasets/validation'):
        shutil.rmtree('../data/KerasDatasets/validation')

    if os.path.isdir('../data/KerasDatasets/test'):
        shutil.rmtree('../data/KerasDatasets/test')

    os.makedirs('../data/KerasDatasets/training')
    os.makedirs('../data/KerasDatasets/validation')
    os.makedirs('../data/KerasDatasets/test')

    # split into training, validation and test set
    # trainings set: 65% of image set
    # validation set: 25% of image set
    # test set: 10% Of image set

    for subject in patches_dict.keys():
        #print(patches_dict[subject])
        sum = len(patches_dict[subject])
        train_count = int(0.65 * sum)
        valid_count = int(0.25*sum)
        test_count = int(0.1 * sum)


        if test_count == 0:
            train_count = train_count - 1
            test_count = 1

        while test_count+valid_count+train_count < sum:
            train_count = train_count + 1

        #print("(train = " + str(train_count) + "; valid = " + str(valid_count) +
        #      "; test = " + str(test_count) + ") = " + str(sum))

        # before splitting the images into the three lists: shuffle the list
        random.shuffle(patches_dict[subject])

        train_imgs = patches_dict[subject][0:train_count]
        valid_imgs = patches_dict[subject][train_count:train_count+valid_count]
        test_imgs = patches_dict[subject][train_count+valid_count:]

        # save the images into a folder structure that can be read by keras
        os.makedirs('../data/KerasDatasets/training/S' + str(subject))
        os.makedirs('../data/KerasDatasets/validation/S' + str(subject))
        os.makedirs('../data/KerasDatasets/test/S' + str(subject))

        for img in train_imgs:
            copyfile("../data/Patches/"+img, '../data/KerasDatasets/training/S' + str(subject) + "/" + img)

        for img in valid_imgs:
            copyfile("../data/Patches/"+img, '../data/KerasDatasets/validation/S' + str(subject) + "/" + img)

        for img in test_imgs:
            copyfile("../data/Patches/"+img, '../data/KerasDatasets/test/S' + str(subject) + "/" + img)


def train_cnn():
    # Calculate how many subjects are available in the dataset based on the number of unique keywords
    subjectFolders = [f for f in listdir("../data/KerasDatasets/validation") if isdir(join("../data/KerasDatasets/validation", f))]
    subjects_count = len(subjectFolders)

    BATCH_SIZE = 32

    # Build a cnn classifier using keras
    # This is based on the vggnet architecture -> Adapted from https://towardsdatascience.com/image-detection-from-scratch-in-keras-f314872006c9
    # Also see https://github.com/tirthajyoti/Deep-learning-with-Python/blob/master/Notebooks/Keras_flow_from_directory.ipynb
    model = Sequential([
        # Note the input shape is the desired size of the image 128x 128 with 3 bytes color
        # The first convolution
        layers.Conv2D(16, (3, 3), activation='relu', input_shape=(128, 128, 3)),
        layers.MaxPooling2D(2, 2),
        # The second convolution
        layers.Conv2D(32, (3, 3), activation='relu'),
        layers.MaxPooling2D(2, 2),
        # The third convolution
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.MaxPooling2D(2, 2),
        # The fourth convolution
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.MaxPooling2D(2, 2),
        # The fifth convolution
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.MaxPooling2D(2, 2),
        # Flatten the results to feed into a dense layer
        layers.Flatten(),
        # 128 neuron in the fully-connected layer
        layers.Dense(128, activation='relu'),
        # 5 output neurons for 5 classes with the softmax activation
        layers.Dense(subjects_count, activation='softmax')
    ])
    # Print the models summary to the console
    model.summary()

    # Set the optimizer and loss function
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    # create image generators to import images from directories
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

    valid_datagen = ImageDataGenerator(rescale=1. / 255)

    train_generator = train_datagen.flow_from_directory(
        directory='../data/KerasDatasets/training',
        target_size=(128, 128),
        color_mode="rgb",
        batch_size=BATCH_SIZE,
        class_mode='categorical')

    validation_generator = valid_datagen.flow_from_directory(
        directory='../data/KerasDatasets/validation',
        target_size=(128, 128),
        color_mode="rgb",
        batch_size=BATCH_SIZE,
        class_mode='categorical')

    # Total sample count
    total_sample = train_generator.n

    # Train the classifier
    model.fit_generator(
        train_generator,
        steps_per_epoch=int(total_sample/BATCH_SIZE),
        epochs=500,
        validation_data=validation_generator,
        validation_steps=800,
        verbose=2)

    # Save the weights for later use


"""
    Trains a convolutional neural network trough transfer learning
"""
def transfer_train_cnn():
    # Calculate how many subjects are available in the dataset based on the number of unique keywords
    subjectFolders = [f for f in listdir("../data/KerasDatasets/validation") if
                      isdir(join("../data/KerasDatasets/validation", f))]
    subjects_count = len(subjectFolders)

    BATCH_SIZE = 32

    # Build upon Mobilenet v1 (See https://towardsdatascience.com/keras-transfer-learning-for-beginners-6c9b8b7143e)
    base_model = MobileNet(weights='imagenet',
                           include_top=False)  # imports the mobilenet model and discards the last 1000 neuron layer.

    x = base_model.output
    x = layers.GlobalAveragePooling2D()(x)
    x = layers.Dense(1024, activation='relu')(
        x)  # we add dense layers so that the model can learn more complex functions and classify for better results.
    #x = layers.Dense(1024, activation='relu')(x)  # dense layer 2
    #x = layers.Dense(512, activation='relu')(x)  # dense layer 3
    preds = layers.Dense(subjects_count, activation='softmax')(x)  # final layer with softmax activation

    # Create a new model that joins the base model with the new layers

    model = Model(inputs=base_model.input, outputs=preds)

    # IMPORTANT: Only train the newly added dense layers
    for layer in model.layers[:88]:
        layer.trainable = False
    for layer in model.layers[88:]:
        layer.trainable = True

    # Print the models summary to the console
    model.summary()

    print("Training starts with layer " + str(model.layers[88].name))

    # Set the optimizer and loss function
    #model.compile(optimizer='adam',
    #model.compile(optimizer=SGD(lr=0.0001, momentum=0.9),
    model.compile(optimizer=SGD(lr=0.0001, momentum=0.9),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    # create image generators to import images from directories
    train_datagen = ImageDataGenerator(
        preprocessing_function=preprocess_input,
        shear_range=0.2,
        zoom_range=0.2,
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True
    )
    valid_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)

    train_generator = train_datagen.flow_from_directory(
        directory='../data/KerasDatasets/training',
        target_size=(224, 224),
        color_mode="rgb",
        batch_size=BATCH_SIZE,
        class_mode='categorical')

    validation_generator = valid_datagen.flow_from_directory(
        directory='../data/KerasDatasets/validation',
        target_size=(224, 224),
        color_mode="rgb",
        batch_size=BATCH_SIZE,
        class_mode='categorical')

    # Total sample count
    total_sample = train_generator.n

    # Train the classifier
    model.fit_generator(
        train_generator,
        steps_per_epoch=int(total_sample/BATCH_SIZE),
        epochs=500,
        validation_data=validation_generator,
        validation_steps=500,
        verbose=2)

    # Save the model for later use
    model.save("model.h5")
    print("Saved Model to file.")
    # It can be loaded with
    #model = load_model('model.h5')

if __name__ == '__main__':
    # This method executes all necessary steps to train a cnn that can be used to classify based on iris

    # If necessary: Extract patches for training from the dataset
    #extract_patches_from_dataset("../data/FullDataset")

    # Splits the patches into a training, validation and test set and saves those into a folder structure
    #convert_patches_into_sets()

    #train the cnn
    #train_cnn()
    transfer_train_cnn()
