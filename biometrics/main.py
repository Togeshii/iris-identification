from biometrics.feature_extraction import FeatureExtraction
from biometrics.clustering import Clustering
from biometrics.preprocessing import Preprocessing
import numpy as np
import os
import pickle
from PIL import Image
import tqdm
import cv2
"""
:arg path (string): path of picture
:return  (): picture
"""
def loadPictures(path):

    return

"""
Identifies iris from an image by comparing it's features with irises from a given database
"""
if __name__ == '__main__':
    # What steps to execute
    extractFeatures = True # If true: Run feature extractor on imageset. Else: Load the specified precomputed features
    generateClusterDatabase = True

    #featuresFile = "20imgs_sift"
    featuresFile = "SmallMaskedDataset_kaze"
    #featuresFile = "20imgs_kaze"

    # Define extraction output
    keypoint_dict = {}

    if extractFeatures:
        # load all images names inside the specified folder into a list
        #imageFolder = "../data/20imgs"
        imageFolder = "../data/SmallDatabase"
        imgList = []
        for file in os.listdir(imageFolder):
            if file.endswith(".tiff") or file.endswith(".jpg"):
                imgList.append(imageFolder + "/" + file)
        #Preprocessing:clahe
        #note: new pictures are getting created, can get errors if not deleting them before running main the second time
        prepro = Preprocessing()
        #imgList = prepro.histogram_equalization(imgList)
        #changedImgList through preprocessing

        # TEST haarcascade

        img_1 = img = cv2.imread(('../data/Pia_test_Haarcascade.JPG'))
        #prepro.haarcascade(img_1)



        extractor = FeatureExtraction()
        #pic = "../data/Kai.jpg"
        keypoint_dict = extractor.extract(imgList, False)

        # save the keypoint dictionary
        with open('../data/'+featuresFile+'.pkl', 'wb') as f:
            pickle.dump(keypoint_dict, f, pickle.HIGHEST_PROTOCOL)
    else:
        with open('../data/'+featuresFile+'.pkl', 'rb') as f:
            keypoint_dict = pickle.load(f)




    """points = np.random.rand(50, 2)*100
    print(keypoint_dict)
    points_pic = keypoint_dict["C5_S2_I13.tiff"]
    points_pic = np.array(points_pic)
    clustering_ex = Clustering(points_pic)


    get_cluster_centers = clustering_ex.k_means(points_pic)"""

    if generateClusterDatabase:
        clustering_ex = Clustering([])

        print("Generating cluster database out of keypoint dictionary")

        # Run clustering on all images in the database and save them to another database
        cluster_database = "../data/small_cluster_database_kaze.pkl"
        cluster_dict = {}

        processed = 0
        for img in keypoint_dict:
            processed = processed + 1
            print("Clustering image " + str(processed) + " / " + str(len(keypoint_dict.keys())))
            centers = clustering_ex.k_means(np.array(keypoint_dict[img]))
            cluster_dict[img] = centers

        # save the keypoint dictionary
        with open(cluster_database, 'wb') as f:
            pickle.dump(cluster_dict, f, pickle.HIGHEST_PROTOCOL)




