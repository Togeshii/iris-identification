import numpy as np
import pickle


"""
Calculates accumulated distance of clustercenter for two eyes.
Therefore it iterates over the to be identified eyes clustercenters and finds the nearest clustercenter of the eye from the database for each
:arg ccenters (np.array): list of clustercenters of unidentified eye
:arg cmp_ccenters (np.array): list of clustercenters of one eye from database
"""

def distance(ccenters, cmp_ccenters):
    # smallest distance for one clustercenter of the to be identified eye
    center_dist = np.inf

    # currently calculated distance between two centers
    current_val = np.inf

    # clustercenter from eye of database with lowest distance
    # has to be removed, so each clustercenter of ccenters only corresponds to one clustercenter in cmp_ccenters
    rmv_center = -1

    # accumulated overall distance between the eyes
    distance = 0

    # iterates over clustercenters of to be identified eye
    for center in ccenters:
        center_dist = np.inf

        # compares one clustercenter of eye to each of current eye from database
        for c in cmp_ccenters:
            # vector between centers
            current_val = np.absolute(center - c)
            # distance between centers
            current_val = np.linalg.norm(current_val)
            # check whether this is closest distance yet
            if (center_dist > current_val):
                center_dist = current_val
                rmv_center = c

        # index of closest clustercenter in cmp_ccenters
        i = np.argwhere(cmp_ccenters == rmv_center)
        # delete center so it doesn't correspond to other clustercenters
        cmp_ccenters = np.delete(cmp_ccenters, i[0, 0], axis=0)

        #add to overall distance
        distance += center_dist
    return distance


"""
:arg ccenters (np.array): clustercenters of the to be identified eye
:arg datapath (stirng): path to database
:arg threshold (int, float, double): maximal allowed distance between eyes
:arg conf_thresh (int, float, double): minimal correleation in percent 
"""
def find_match(ccenters, datapath='../data/cluster_database_sift.pkl', threshold=80, conf_thresh=70):
    # extract database
    with open(datapath, 'rb') as f:
        database = pickle.load(f)
        print("Datbase length " + str(len(database)))

    # list of found matches
    matches = []
    # distance between eyes
    dist = -1
    # confidence of match
    confidence = -1
    # iterate over images in database
    for img in database:
        # sometimes database contains empty list of clustercenters. This is supposed to prevent calculating distance for those
        if (len(database[img]) == 0):
            print("No clustercenters found for", img)
        else:
            # distance (or in other words correlation) of to be identified eye and current eye from database
            dist = distance(ccenters, database[img])

            if (threshold > dist):
                # calculate confidence for eyes with low distance
                confidence = 100 - (100 * dist / threshold)
                # add eye to matches if confidence is high enough
                if(confidence>=conf_thresh):
                    matches.append([img, confidence])

    # sort for decreasing order of confidence
    matches.sort(key=lambda x:x[1])
    matches=np.reshape(matches, (len(matches),2))

    print(matches)
    return matches