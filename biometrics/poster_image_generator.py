from biometrics import preprocessing, feature_extraction, clustering, feature_extraction
import pickle
from biometrics.attack_detector import AttackDetector
import cv2
import numpy as np
from cv2 import *

prepro = preprocessing.Preprocessing()
"""img, right_eye = prepro.haarcascade(cv2.imread("../data/DSC_1476.JPG"))
cv2.imwrite("../data/MarkedEye.png", img)
cv2.imwrite("../data/ExtractedRightEye.png", right_eye)"""

"""clahe = cv2.createCLAHE(clipLimit=2,tileGridSize=(8, 8) )
gray = cv2.cvtColor(cv2.imread("../data/SegmentedEye.JPG"), cv2.COLOR_BGR2GRAY)
img = clahe.apply(gray)



cv2.imwrite("../data/claheVis.png", img)"""

extractor = feature_extraction.FeatureExtraction()
keypoint_dict = extractor.extract(["../data/claheVis.png"], False)
points_pic = np.array(keypoint_dict["claheVis.png"])
clustering_ex = clustering.Clustering(points_pic)

cluster_centers = clustering_ex.k_means(points_pic)

print(cluster_centers)

im = cv2.imread("../data/claheVis.png")
for cen in cluster_centers:
    cv2.circle(im, (int(cen[0]), int(cen[1])), 4, (0, 0, 255), -1)

#cv2.imshow("Cluster centers", im)
#cv2.waitKey(0)

cv2.imwrite("../data/clustercenters.png", im)

