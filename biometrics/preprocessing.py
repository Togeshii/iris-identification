import numpy as np
import cv2
from matplotlib import pyplot as plt
from PIL import Image
import flask

class Preprocessing:

    def haarcascade(self, img):
        face_cascade = cv2.CascadeClassifier('../data/haarcascade_frontalface_default.xml')
        eye_cascade = cv2.CascadeClassifier('../data/haarcascade_eye.xml')

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        #noch ändern
        right_eye_img = np.zeros_like(img)
        for (x, y, w, h) in faces:
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

            roi_gray = gray[y:y + h, x:x + w]
            roi_color = img[y:y + h, x:x + w]
            eyes = eye_cascade.detectMultiScale(roi_gray)

            right_eyes_List = []
            for (ex, ey, ew, eh) in eyes:
                #find eye in left half of the image
                if ex <  roi_gray.shape[0]/2:
                    right_eyes_List.append((ex, ey, ew, eh))

                cv2.rectangle(roi_color, (ex - 2, ey - 2), (ex + ew + 4 , ey + eh + 4), (0, 255, 0), 2)
            (ex, ey, ew, eh) = right_eyes_List[0]
            right_eye = (ex, ey, ew, eh)
            #find the 'highest eye'
            for (ex, ey, ew, eh) in right_eyes_List:
                #Bedingung unklar, aber es passt
                if ey < right_eye[1]:
                    right_eye = (ex, ey, ew, eh)

            (ex, ey, ew, eh) = right_eye
            cv2.rectangle(roi_color, (ex - 2, ey - 2), (ex + ew + 4, ey + eh + 4), (0, 0, 255), 2)

            #cut out right eye
            right_eye_img = img[y+ey:y+eh+ey, x+ex:x+ ew+ex, :]


        cv2.imshow('img', img)
        if right_eye_img.shape == img.shape:
            right_eye_img = None
            print('No face found, take new picture!')
        else:
            cv2.imshow('right eye', right_eye_img)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()
        # while True:
        #     #close window
        #     k = cv2.waitKey(1)
        #     if k % 256 == 32:
        #         # ESC pressed
        #         print("Escape hit, closing...")
        #         break
        return img, right_eye_img

    def histogram_equalization(self, images):

        for i in range(0, len(images)):

            #load image
            img = np.array(Image.open(images[i]).convert('L'))

            #using clahe
            clahe = cv2.createCLAHE(clipLimit=2,tileGridSize=(8, 8) )
            img = clahe.apply(img)

            #creating copy of clahe version
            #TODO:maybe replacing String
            path = images[i].replace('.png', '_clahe.png')

            cv2.imwrite(path, img)
            #new path in images array
            images[i] = path
        return images





