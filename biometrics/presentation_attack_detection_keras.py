import os
from os import listdir
from os.path import isfile, join, isdir
import cv2
import numpy as np
from tqdm import tqdm
from sklearn.cluster import KMeans
from keras import layers
from keras.models import Sequential
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import TensorBoard
import random
import shutil
from shutil import copyfile
from keras.applications import MobileNet
from keras.preprocessing import image
from keras.applications.mobilenet import preprocess_input
from keras.models import Model
from keras.optimizers import SGD
from keras.models import load_model

def extract_patches_from_dataset(datasetPath, outputPath):
    # Some settings
    DEBUG_VISUALIZE_REGIONS_OF_INTEREST = False
    DEBUG_VISUALIZE_EXTRACTED_EYES = False
    DEBUG_VISUALIZE_SCALED_AND_NORMALIZED_EYES = False

    # Obtain a list of all training images
    images = [f for f in listdir(datasetPath) if isfile(join(datasetPath, f)) and str(f)[-3:] == "jpg"]

    print("Found " + str(len(images)) + " source images")

    # Copied from preprocessing.py
    face_cascade = cv2.CascadeClassifier('../data/haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier('../data/haarcascade_eye.xml')

    saved_images = 0

    # For each image
    for img_id in tqdm(range(len(images))):
        img_name = images[img_id]

        # Load the image
        img = cv2.imread(datasetPath + "/" + img_name)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        eye = None
        for (x, y, w, h) in faces:
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = img[y:y + h, x:x + w]
            eyes = eye_cascade.detectMultiScale(roi_gray)

            if len(eyes) == 0:
                continue

            # Choose the region whose center is nearest to the expected position of the left eye
            approxPosX = x + 0.3*w
            approxPosY = y+0.4*h
            cv2.circle(img, (int(approxPosX), int(approxPosY)), 5, (0, 0, 255), -1)

            bestDist = 1000
            bestID = -1
            # loop trough the eyes and choose the one that is nearest
            for i in range(len(eyes)):
                (ex, ey, ew, eh) = eyes[i]
                cx = x+ex+0.5*ew
                cy = y+ey+0.5*eh

                dist = np.sqrt((approxPosX-cx)*(approxPosX-cx) + (approxPosY-cy)*(approxPosY-cy))

                if dist < bestDist:
                    bestDist = dist
                    bestID = i

            (ex, ey, ew, eh) = eyes[bestID]
            eye = (x+ex, y+ey, ew, eh)

            for i in range(len(eyes)):
                (ex, ey, ew, eh) = eyes[i]

                if i == bestID:
                    cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 0, 255), 2)
                else:
                    cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)


        if DEBUG_VISUALIZE_REGIONS_OF_INTEREST:
            cv2.imshow('img', img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        if eye is None:
            continue

        # Extract the region of interest as pixel image
        img_fresh = cv2.imread(datasetPath + "/" + img_name)
        eye_col = img_fresh[eye[1]:eye[1] + eye[3], eye[0]:eye[0] + eye[2]]

        if DEBUG_VISUALIZE_EXTRACTED_EYES:
            cv2.imshow("Extracted Eye", eye_col)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        # Rescale the extracted eye
        scaled = cv2.resize(eye_col, (128, 128))
        cv2.imwrite(outputPath + "/" + "{:03d}".format(saved_images) + ".jpg", scaled)
        saved_images = saved_images + 1

def convert_patches_into_sets(input_path, output_path):
    # Obtain a list of all training images
    patches = [f for f in listdir(input_path) if isfile(join(input_path, f)) and str(f)[-3:] == "jpg"]
    print("Found " + str(len(patches)) + " labeled patches")

    patches_dict = {}
    patches_dict["real"] = []
    patches_dict["fake"] = []

    for patch in patches:
        # get the subject id from the patch name
        id = int(str(patch).split(".")[0])

        if id < 115:
            patches_dict["real"].append(patch)
        elif id < 284:
            patches_dict["fake"].append(patch)
        elif id < 385:
            patches_dict["real"].append(patch)
        elif id < 452:
            patches_dict["fake"].append(patch)
        elif id < 734:
            patches_dict["real"].append(patch)
        else:
            patches_dict["fake"].append(patch)



    # Clear any previously existing folder structure
    if os.path.isdir(output_path + '/training'):
        shutil.rmtree(output_path + '/training')

    if os.path.isdir(output_path + '/validation'):
        shutil.rmtree(output_path + '/validation')

    if os.path.isdir('../data/KerasDatasets_PAD/test'):
        shutil.rmtree('../data/KerasDatasets_PAD/test')

    os.makedirs(output_path + '/training')
    os.makedirs(output_path + '/validation')
    os.makedirs(output_path +  '/test')

    # split into training, validation and test set
    # trainings set: 65% of image set
    # validation set: 25% of image set
    # test set: 10% Of image set

    for subject in patches_dict.keys():
        #print(patches_dict[subject])
        sum = len(patches_dict[subject])
        train_count = int(0.8 * sum)
        valid_count = int(0.15*sum)
        test_count = int(0.05 * sum)


        if test_count == 0:
            train_count = train_count - 1
            test_count = 1

        while test_count+valid_count+train_count < sum:
            train_count = train_count + 1

        #print("(train = " + str(train_count) + "; valid = " + str(valid_count) +
        #      "; test = " + str(test_count) + ") = " + str(sum))

        # before splitting the images into the three lists: shuffle the list
        random.shuffle(patches_dict[subject])

        train_imgs = patches_dict[subject][0:train_count]
        valid_imgs = patches_dict[subject][train_count:train_count+valid_count]
        test_imgs = patches_dict[subject][train_count+valid_count:]

        # save the images into a folder structure that can be read by keras
        os.makedirs(output_path + '/training/' + str(subject))
        os.makedirs(output_path + '/validation/' + str(subject))
        os.makedirs(output_path + '/test/' + str(subject))

        for img in train_imgs:
            copyfile(input_path + "/" + img, output_path + '/training/' + str(subject) + "/" + img)

        for img in valid_imgs:
            copyfile(input_path + "/" + img, output_path + '/validation/' + str(subject) + "/" + img)

        for img in test_imgs:
            copyfile(input_path + "/" + img, output_path + '/test/' + str(subject) + "/" + img)

def transfer_train_cnn():
    # Calculate how many subjects are available in the dataset based on the number of unique keywords
    subjectFolders = [f for f in listdir("../data/KerasDatasets_PAD/validation") if
                      isdir(join("../data/KerasDatasets_PAD/validation", f))]
    subjects_count = len(subjectFolders)

    BATCH_SIZE = 32

    # Build upon Mobilenet v1 (See https://towardsdatascience.com/keras-transfer-learning-for-beginners-6c9b8b7143e)
    base_model = MobileNet(weights='imagenet',
                           include_top=False, input_shape=(128, 128, 3))  # imports the mobilenet model and discards the last 1000 neuron layer.

    x = base_model.output
    x = layers.GlobalAveragePooling2D()(x)
    x = layers.Dense(256, activation='relu')(
        x)  # we add dense layers so that the model can learn more complex functions and classify for better results.
    #x = layers.Dense(256, activation='relu')(x)  # dense layer 2
    #x = layers.Dense(128, activation='relu')(x)  # dense layer 3
    preds = layers.Dense(subjects_count, activation='softmax')(x)  # final layer with softmax activation

    # Create a new model that joins the base model with the new layers

    model = Model(inputs=base_model.input, outputs=preds)

    # OFFSET
    #offset = 9 # For laptop
    offset = 0 # For Pc

    # IMPORTANT: Only train the newly added dense layers
    for layer in model.layers[:88+offset]:
        layer.trainable = False
    for layer in model.layers[88+offset:]:
        layer.trainable = True

    # Print the models summary to the console
    model.summary()

    print("Available layers = " + str(len(model.layers)))
    print("Training starts with layer " + str(model.layers[88+offset].name))

    # Set the optimizer and loss function
    #model.compile(optimizer='adam',
    #model.compile(optimizer=SGD(lr=0.0001, momentum=0.9),
    model.compile(optimizer=SGD(lr=0.0001, momentum=0.9),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    # create image generators to import images from directories
    train_datagen = ImageDataGenerator(
        preprocessing_function=preprocess_input,
        zoom_range=0.2,
        rotation_range=30
    )
    valid_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)

    train_generator = train_datagen.flow_from_directory(
        directory='../data/KerasDatasets_PAD/training',
        target_size=(128, 128),
        color_mode="rgb",
        batch_size=BATCH_SIZE,
        class_mode='categorical')

    validation_generator = valid_datagen.flow_from_directory(
        directory='../data/KerasDatasets_PAD/validation',
        target_size=(128, 128),
        color_mode="rgb",
        batch_size=BATCH_SIZE,
        class_mode='categorical')

    # Total sample count
    total_sample = train_generator.n

    # Train the classifier
    model.fit_generator(
        train_generator,
        steps_per_epoch=int(total_sample/BATCH_SIZE),
        epochs=500,
        validation_data=validation_generator,
        validation_steps=500,
        verbose=2)

    # Save the model for later use
    model.save("model.h5")
    print("Saved Model to file.")
    # It can be loaded with
    #model = load_model('model.h5')


if __name__ == '__main__':
    # Extract eye patches from all images
    #extract_patches_from_dataset("../data/KaiWebcam", "../data/Patches_Kai")

    # Convert the patches into a folder structure that is readable by keras
    #convert_patches_into_sets("../data/Patches_Kai", "../data/KerasDatasets_PAD_Big")

    # Train a neural network
    transfer_train_cnn()