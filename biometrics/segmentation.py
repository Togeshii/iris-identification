import cv2
import numpy as np
import ntpath
import os
from os import listdir
from os.path import isfile, join, isdir
from tqdm import tqdm
from joblib import Parallel, delayed


def mask_iris(image_path, output_path, mask_path):
    DEBUG_VISUALIZE_IRIS_CENTER = True
    DEBUG_VISUALIZE_GRADIENT = False
    DEBUG_VISUALIZE_DETECTED_CIRCLE = True
    DEBUG_VISUALIZE_EXTRACTED_IRIS = False
    DEBUG_VISUALIZE_MASK = False

    EXPORT_MASKED_IMAGE = True
    EXPORT_MASK = False
    REFINE_IRIS_POSITION = False



    # First try to find the pupil using thresholding
    # Normalize the image before thresholding
    img = cv2.imread(image_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)



    # using clahe from preprocessing.py
    clahe = cv2.createCLAHE(clipLimit=2, tileGridSize=(8, 8))
    gray = clahe.apply(gray)

    iris_center = (-1, -1)

    # Use multiple elipses as mask to start searching the iris in the middle of the image
    """for radius in range(20, 200, 10):
        workCopy = gray[:].copy()
        # Mask the image with an ellipse
        cv2.ellipse(workCopy, (int(workCopy.shape[1] / 2), int(workCopy.shape[0] / 2)),
                    (int(radius) + 300, int(0.5 * radius) + 300),
                    0, 0, 360, 255, 600)

        # Loop trough different thresholds in steps of 5 until sufficient pixels were found
        for thr in range(0,80,5):
            ret, thresholded_img = cv2.threshold(workCopy, thr, 255, cv2.THRESH_BINARY)

            # Loop trough all black pixels and calculate the center
            # grab the image dimensions
            h = thresholded_img.shape[0]
            w = thresholded_img.shape[1]

            x_sum = 0
            y_sum = 0
            num = 0

            # loop over the image, pixel by pixel
            for y in range(0, h):
                for x in range(0, w):
                    if thresholded_img[y, x] < 30:
                        num = num + 1
                        x_sum = x_sum + x
                        y_sum = y_sum + y

            # If less than 25 pixels were found we do not want to use the result
            #if num < w/50:
            #    continue

            if num < 10:
                continue

            iris_center = (x_sum/num, y_sum/num)

            # Do not continue searching
            break

        if iris_center != (-1, -1):
            break
            """

    iris_center = (gray.shape[0]/2, gray.shape[1]/2)

    # If the iris center has been found: Try to refine it further by doing another more closely masked threshold
    if iris_center != (-1, -1) and REFINE_IRIS_POSITION:
        workCopy = gray[:].copy()
        cv2.circle(workCopy, (int(iris_center[0]), int(iris_center[1])), int(workCopy.shape[1] / 10) + 200, (255, 255, 255), 400)
        ret, thresholded_img = cv2.threshold(workCopy, 50, 255, cv2.THRESH_BINARY)

        h = thresholded_img.shape[0]
        w = thresholded_img.shape[1]

        x_sum = 0
        y_sum = 0
        num = 0
        # loop over the image, pixel by pixel
        for y in range(0, h):
            for x in range(0, w):
                if thresholded_img[y, x] < 50:
                    num = num + 1
                    x_sum = x_sum + x
                    y_sum = y_sum + y

        if num != 0:
            iris_center = (x_sum / num, y_sum / num)

            if DEBUG_VISUALIZE_IRIS_CENTER:
                cv2.circle(img, (int(iris_center[0]), int(iris_center[1])), 5, (0, 0, 255), -1)
                cv2.imshow('Detected iris', img)
                #cv2.waitKey(0)
                #cv2.destroyAllWindows()

    # Run an edge detection on the image
    grad_x = cv2.Sobel(gray, cv2.CV_16S, 1, 0, ksize=3, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT)
    grad_y = cv2.Sobel(gray, cv2.CV_16S, 0, 1, ksize=3, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT)

    abs_grad_x = cv2.convertScaleAbs(grad_x)
    abs_grad_y = cv2.convertScaleAbs(grad_y)

    grad = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)

    if DEBUG_VISUALIZE_GRADIENT:
        cv2.imshow('Gradient', grad)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    # Optimization
    best_radius = -1
    best_score = -1

    # Calculate the maximal possible radisu
    max_radius = img.shape[0]/2
    max_radius = min(max_radius, iris_center[1]) # Distance to left border
    max_radius = min(max_radius, img.shape[1] - iris_center[1]) # Distance to right border
    max_radius = min(max_radius, img.shape[0] - iris_center[0]) # Distance to bottom border
    max_radius = min(max_radius, img.shape[0]) # Distance to top border

    # Try different circles around the pupil and calculate the one with the highest gradient
    for radius in np.arange(start=int(img.shape[0]/5), stop=max_radius, step=0.5):
        # Calculate the summed gradient
        grad_sum = 0
        counted_pixels = 0

        # Loop trough the points on a circle by 1 degree increments
        for phi in range(360):
            # Calculate the theoretical point
            point = [iris_center[1]+radius*np.cos(np.deg2rad(phi)),iris_center[0]+radius*np.sin(np.deg2rad(phi))]
            # Calculate the pixel position
            px = [int(point[0]), int(point[1])]

            # If the position is inside the image
            if px[0] >= 0 and px[0] < img.shape[0] and px[1] >= 0 and px[1] < img.shape[1]:
                grad_sum = grad_sum + grad[px[0], px[1]]
                counted_pixels = counted_pixels + 1

                #img[px[0], px[1]] = (0, 0, 255)
                #cv2.imshow('Drawn pixel', img)
                #cv2.waitKey(0)

        # Calculate the gradient per pixel to measure the overall performance of the radius
        if counted_pixels > 0:
            grad_per_px = grad_sum / counted_pixels
            #print("Radius = " + str(radius) + " -> Gradient per pixel = " + str(grad_per_px))

            if grad_per_px > best_score:
                best_radius = radius
                best_score = grad_per_px


    if DEBUG_VISUALIZE_DETECTED_CIRCLE:
        print("Best radius = " + str(best_radius))
        cv2.circle(img, (int(iris_center[0]), int(iris_center[1])), int(best_radius), (0, 0, 255), 1)
        cv2.imshow('Detected Iris', img)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()

    # Calculate the masked image
    if EXPORT_MASKED_IMAGE:
        padding = 5
        only_iris = cv2.imread(image_path)
        cv2.circle(only_iris, (int(iris_center[0]), int(iris_center[1])), int(best_radius)+500, (127, 127, 127), 1000)
        only_iris = only_iris[int(iris_center[1])-int(best_radius)-padding:int(iris_center[1])+int(best_radius)+padding,
                int(iris_center[0]) - int(best_radius)-padding:int(iris_center[0]) + int(best_radius)+padding]

        if not only_iris.size == 0:
            iris_gray = cv2.cvtColor(only_iris, cv2.COLOR_BGR2GRAY)
            clahe.apply(iris_gray)

            if DEBUG_VISUALIZE_EXTRACTED_IRIS:
                cv2.imshow('Extracted Iris', iris_gray)
                cv2.waitKey(0)
                cv2.destroyAllWindows()

            cv2.imwrite(output_path, iris_gray)

    # Also export the mask
    if EXPORT_MASK:
        only_iris = cv2.imread(image_path)

        cv2.circle(only_iris, (0, 0), 500, (255, 255, 255), 1000)
        cv2.circle(only_iris, (int(iris_center[0]), int(iris_center[1])), int(best_radius) + 500, (0, 0, 0), 1000)

        only_iris = cv2.cvtColor(only_iris, cv2.COLOR_BGR2GRAY)

        if DEBUG_VISUALIZE_MASK:
            cv2.imshow('Mask', only_iris)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        cv2.imwrite(mask_path, only_iris)


def convert_single_dataset_entry(image):
    imgname = ntpath.basename(image)
    mask_iris(image, "../data/MaskedPatches/" + imgname, "../data/Masks/" + imgname)



# Batch convert dataset
def batch_convert_dataset(datasetPath):
    images = [str(os.path.abspath(join(datasetPath, f))) for f in listdir(datasetPath) if isfile(join(datasetPath, f)) and str(f)[-4:] == "tiff"]

    Parallel(n_jobs=-1, verbose=15, backend="multiprocessing")(
        map(delayed(convert_single_dataset_entry), images))

    #for imgID in tqdm(range(len(images))):
        #convert_single_dataset_entry(images[imgID])


if __name__ == '__main__':
    #mask_iris("../data/Patches_Kai/004.jpg","../data/MaskedPatches/004.jpg")
    #mask_iris("../data/FullDataset/C20_S1_I10.tiff","../data/MaskedPatches/C20_S1_I10.jpg")
    #mask_iris("../data/FullDataset/C10_S1_I2.tiff","../data/MaskedPatches/C10_S1_I2.jpg")
    #mask_iris("../data/FullDataset/C211_S1_I5.tiff","../data/MaskedPatches/C211_S1_I5.jpg")
    #mask_iris("../data/FullDataset/C100_S1_I12.tiff","../data/MaskedPatches/C100_S1_I12.jpg")

    batch_convert_dataset("../data/FullDataset")
