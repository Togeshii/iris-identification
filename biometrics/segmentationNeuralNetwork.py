import tensorflow as tf
from PIL import Image, ImageOps
import numpy as np
import cv2
from keras_segmentation.models.unet import vgg_unet

"""
    Python class that loads a pretrained keras model that classifies the input as presentation attack.
    The code is based on the example code by "TeachableMachine" created by Google
"""
class Segmentation:
    def loadModel(self):
        # Load the model
        #self.model = tf.keras.models.load_model('segmentationModel.h5')
        self.model = vgg_unet(n_classes=2 ,  input_height=128, input_width=128  )
        self.model.load_weights("../biometrics/segmentationModel.h5")

    def predict(self, imagePath, outputPath):
        # First rescale the image for input into the network
        image = Image.open(imagePath)
        size = (128, 128)
        image = ImageOps.fit(image, size, Image.ANTIALIAS)

        image.save("../data/tmp/resized.png", "PNG")

        self.model.predict_segmentation(
            inp="../data/tmp/resized.png",
            out_fname="../data/tmp/mask.png"
        )

        # Load the image and mask
        img = cv2.imread("../data/tmp/resized.png")
        mask = cv2.imread("../data/tmp/mask.png")
        mask_gray = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)

        ret, mask_bw = cv2.threshold(mask_gray, 170, 255, cv2.THRESH_BINARY)


        # First roughly loop over the image to calculate the average color
        summed_brightness = 0
        count = 0

        for y in range(0, img.shape[0], 5):
            for x in range(0, img.shape[1], 5):
                if mask_bw[y, x] > 50:
                    summed_brightness += img[y, x, 0]
                    summed_brightness += img[y, x, 1]
                    summed_brightness += img[y, x, 2]
                    count += 3

        avg = summed_brightness/count

        # loop over the image, pixel by pixel
        for y in range(0, img.shape[0]):
            for x in range(0, img.shape[1]):
                if mask_bw[y, x] < 50:
                    img[y, x] = (avg, avg, avg)

        cv2.imshow('Segmented Image', img)
        #cv2.waitKey(0)
        cv2.imwrite(outputPath, img)




if __name__ == '__main__':
    seg = Segmentation()
    seg.loadModel()
    seg.predict("../data/Patches/16_11.jpg", "../data/tmp/test.png")

