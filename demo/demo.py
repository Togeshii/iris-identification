import os
import numpy as np
import pickle
import keras
from keras.models import load_model
from scipy.cluster.vq import vq, whiten
import matplotlib.pyplot as plt

import cv2

os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

class KeyPoint(object):

    def __init__(self, kp):
        # type: (cv2.KeyPoint) -> None
        x, y = kp.pt
        self.pt = float(x), float(y)
        self.angle = float(kp.angle) if kp.angle is not None else None
        self.size = float(kp.size) if kp.size is not None else None
        self.response = float(kp.response) if kp.response is not None else None
        self.class_id = int(kp.class_id) if kp.class_id is not None else None
        self.octave = int(kp.octave) if kp.octave is not None else None
        self.class_rel = None

    def to_opencv(self):
        # type: () -> cv2.KeyPoint
        kp = cv2.KeyPoint()
        kp.pt = self.pt
        kp.angle = self.angle
        kp.size = self.size
        kp.response = self.response
        kp.octave = self.octave
        kp.class_id = self.class_id
        return kp

def find_bbx(img, eye_cascade):
    eyes = eye_cascade.detectMultiScale(img)
    if eyes == ():
        return None
    index_1 = np.argmax(eyes[:, 2])
    index_2 = np.argmax(eyes[:, 3])
    if index_1 == index_2:
        eye = eyes[index_1]
    else:
        roi1 = eyes[index_1][2] * eyes[index_1][3]
        roi2 = eyes[index_2][2] * eyes[index_2][3]
        if roi1 > roi2:
            eye = eyes[index_1]
        else:
            eye = eyes[index_2]

    return eye


def read_models(model_path):
    num_cluster = 3
    codebook_path = os.path.join(model_path, 'codebook.npy')
    regression_model_path = os.path.join(model_path, 'regression_model.sav')

    codebook = np.load(codebook_path)

    models = []
    for i in range(num_cluster):
        model_name = str(i) + '_cnn.h5'
        cnn_path = os.path.join(model_path, model_name)

        def _hard_swish(x):
            return x * keras.backend.relu(x + 3.0, max_value=6.0) / 6.0

        def _relu6(x):
            return keras.backend.relu(x, max_value=6.0)

        models.append(load_model(cnn_path, custom_objects={'_hard_swish': _hard_swish, '_relu6': _relu6}))

    lr = pickle.load(open(regression_model_path, 'rb'))

    return codebook, models, lr


def process_image(frame, codebook, models, lr, roi, eye_cascade, plot_keypoints=False):
    frame_copy = frame.copy()
    # 1. detect eyes and generator mask
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    eyes = find_bbx(frame, eye_cascade)
    mask = np.zeros((frame.shape[0], frame.shape[1]), dtype=np.uint8)

    for (ex, ey, ew, eh) in [eyes]:
        iris_w = int(ex + float(ew / 2))
        iris_h = int(ey + float(eh / 2))
        ratio = int(np.min(np.array(ew, eh)) / 4)
        cv2.circle(mask, (iris_w, iris_h), ratio, (255, 255, 255), -1, 8, 0)
        _, mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)

    # 2. SIFT keypoints detection and assgin cluster ID
    sift = cv2.xfeatures2d.SIFT_create()
    clahe = cv2.createCLAHE(clipLimit=10.0, tileGridSize=(10, 10))
    cv2.normalize(frame, frame, 0, 255, cv2.NORM_MINMAX)
    frame = clahe.apply(frame)
    kp, des = sift.detectAndCompute(frame, mask)
    # 2.1 plot sift keypoints
    if plot_keypoints is True:
        img_kps = cv2.drawKeypoints(frame_copy, kp, None, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        plt.imshow(img_kps, cmap='gray', interpolation='bicubic')
        plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        plt.show()

    temp = sorted(zip(kp, des), key=lambda kp: kp[0].response, reverse=True)
    sorted_kp, des = map(list, zip(*temp))
    kps = [KeyPoint(k) for k in sorted_kp]
    whitened = whiten(des)
    labels, dist = vq(whitened, codebook)
    assert max(dist) != min(dist), 'max equal to min'
    normalized = (dist - min(dist)) / (max(dist) - min(dist))
    for i in range(len(kps)):
        kps[i].class_id = labels[i]
        kps[i].class_rel = normalized[i]

    # 3. prediction
    combined_data = []
    predict_data = [list() for _ in range(len(models))]
    class_rel_data = [list() for _ in range(len(models))]
    from tqdm import tqdm
    for kp in kps:
        indx = kp.class_id  # cluster id
        x, y, kp_class_rel = kp.pt[0], kp.pt[1], kp.class_rel
        img_roi = frame[int(y - roi/2): int(y + roi/2), int(x - roi/2): int(x + roi/2)]
        if img_roi.shape != (roi, roi):
            continue
        img_roi = img_roi.reshape(roi, roi, 1)
        predict_data[indx].append(img_roi)
        class_rel_data[indx].append(kp_class_rel)

    for indx, data in enumerate(predict_data):
        data = np.array(data).reshape(len(data), roi, roi, 1)
        if data.shape[0] == 0:
            continue
        pred = models[indx].predict(data)
        for j in range(pred.shape[0]):
            combined_data.append(np.array([pred[j][1], class_rel_data[indx][j], indx]))

    combined_data = np.vstack(combined_data)
    scores = lr.predict_proba(combined_data)
    score = np.mean(scores[:, 1])

    return kps, score, eyes


def draw_keypoints(img, keypoints, label):
    height, width = img.shape[0], img.shape[1]
    # yellow, blue, red, megenta, cyan
    colors = [(255, 255, 0), (0, 0, 255), (255, 0, 0), (255, 0, 255), (0, 255, 255), (128, 0, 0), (128, 128, 0),
              (192, 192, 192), (0, 128, 128), (128, 0, 128)]
    for kp in keypoints[0:20]:
        x, y = kp.pt
        class_id = kp.class_id
        xmin, ymin = int(x - 24), int(y - 24)
        xmax, ymax = int(x + 24), int(y + 24)
        if xmin >= 0 or ymin >= 0 or xmax <= width or ymax <= height:
            cv2.rectangle(img, (xmin, ymin), (xmax, ymax), colors[int(class_id)], 1)
        else:
            continue

    cv2.putText(img, 'label: %s ' % label, (width - 100, height - 100),
                cv2.FONT_HERSHEY_SIMPLEX, 0.9, (36, 255, 12), 2)
    cv2.imshow('image', img)

def main():
    video_path = 'videos/attack/Spoof_Sr_Ip_Cp_Nk_33.avi'
    model_path = 'models'
    threshold = 0.7
    eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

    codebook, models, lr = read_models(model_path)

    cap = cv2.VideoCapture(video_path)
    frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    for i in range(int(frames) - 1):
        flag, frame = cap.read()
        kps, score, eyes = process_image(frame, codebook, models, lr, 48, eye_cascade)

        if score >= threshold:
            label = 'live'
        else:
            label = 'spoof'

        colors = [(255, 255, 0), (0, 0, 255), (255, 0, 0)]
        for kp in kps[0:40]:
            x, y = kp.pt
            class_id = kp.class_id
            xmin, ymin = int(x - 24), int(y - 24)
            xmax, ymax = int(x + 24), int(y + 24)
            if xmin >= 0 or ymin >= 0 or xmax <= frame.shape[1] or ymax <= frame.shape[0]:
                cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), colors[int(class_id)], 2)
                cv2.putText(frame, 'rel: %s ' % str(kp.class_rel)[0:4], (xmin, ymin - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.4, (36, 255, 12), 1)

            else:
                continue

        cv2.rectangle(frame, (eyes[0], eyes[1]), (eyes[0] + eyes[2], eyes[1] + eyes[3]), (0, 255, 0), 2)
        cv2.putText(frame, 'label: %s, s: %s ' % (label, str(score)[0:6]), (eyes[0], eyes[1] - 20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.8, (36, 255, 12), 2)
        cv2.imshow('frame', frame)
        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break

if __name__ == '__main__':
    main()